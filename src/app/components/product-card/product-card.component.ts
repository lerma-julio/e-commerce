import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.css']
})
export class ProductCardComponent implements OnInit {

  @Input() product: any;
  @Input() isCart = false;

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
  }

  onClick(){
    this.apiService.myCart.push(this.product);
  }

  onDeleteClick(){
    const element = this.apiService.myCart.find(x => x === this.product);
    console.log(element);
    const index = this.apiService.myCart.indexOf(element);
    console.log(index);
    
    this.apiService.myCart.splice(index, 1)

    //this.apiService.myCart.splice(index, index)
    console.log(this.apiService.myCart);
  }
}
